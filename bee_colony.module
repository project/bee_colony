<?php 
// $Id:$


/**
 * @file
 * Server for beeWorker, an Ubercart sincronization framework.
 */

/**
 * Determine the widget's files directory
 *
 * @param $field
 *   A CCK field array.
 * @param $account
 *   The user account object to calculate the file path for.
 * @return
 *   The files directory path, with any tokens replaced.
 */
function bee_colony_file_path($field, $account = NULL) {
  $account = isset($account) ? $account : $GLOBALS['user'];
  $dest = $field['widget']['file_path'];
  // Replace user level tokens.
  // Node level tokens require a lot of complexity like temporary storage
  // locations when values don't exist. See the filefield_paths module.
  if (module_exists('token')) {
    $dest = token_replace($dest, 'user', $account);
  }
  // Replace nasty characters in the path if possible.
  if (module_exists('transliteration')) {
    module_load_include('inc', 'transliteration');
    $dest_array = array_filter(explode('/', $dest));
    foreach ($dest_array as $key => $directory) {
      $dest_array[$key] = transliteration_clean_filename($directory);
    }
    $dest = implode('/', $dest_array);
  }
  $return = file_directory_path();
  if ($dest != '') $return.= '/'. $dest;
  return $return;
}

/**
 * Implementation of hook_perm().
 */
function bee_colony_perm() {
  return array(
    'Administer Bee Colony',
    'View orders',
    'Modify orders',
    'Purge taxonomy',
    'Administer catalog products',
  );
}

/*
* Implementation of hook_disable().
* 
* Perform necessary actions before module is disabled (like clear the services:methods cache).
*/
function bee_colony_disable() {
  cache_clear_all('services:methods', 'cache');
}
/**
* Implementation of hook_disable()
* 
* Perform necessary actions after module is enabled (like clear the services:methods cache).
*/
function bee_colony_enable() {
  cache_clear_all('services:methods', 'cache');
}

/**
* Implementation of hook_service().
*/
function bee_colony_service() {
  return array(
    // beeColony.ordersGetByStatus
    array(
      '#method'   => 'beeColony.ordersGetByStatus',
      '#callback' => 'xmls_bee_colony_orders_get_by_status',  //FIXED
      '#access arguments' => array('View orders'),
      '#return'   => 'struct',
      '#file'    => array('file' => 'orders.inc', 'module' => 'bee_colony'),
      '#args'     => array(
        array(
          '#name'         => 'status',
          '#type'         => 'string',
          '#description'  => t('Order status.'),
        ),
      ),
      '#help'     => t('Returns xml string for orders actually in given status.'),
    ),
    
    // bee_colony.getAllOrders
    array(
      '#method'   => 'beeColony.ordersGetAll',
      '#callback' => 'xmls_bee_colony_orders_get_all', //FIXED
      '#access arguments' => array('View orders'),
      '#return'   => 'struct',
      '#file'    => array('file' => 'orders.inc', 'module' => 'bee_colony'),
      '#help'     => t('Returns array for orders for all statuses.'),
    ),
    
    // bee_colony.orderUpdate
    array(
      '#method'   => 'beeColony.orderUpdate',
      '#callback' => 'xmls_bee_colony_order_update',  //FIXED
      '#access arguments' => array('Modify orders'),
      '#return'   => 'struct',
      '#file'    => array('file' => 'orders.inc', 'module' => 'bee_colony'),
      '#args'     => array(
        array(
          '#name'         => 'id',
          '#type'         => 'int',
          '#description'  => t('Order ID number.'),
        ),
        array(
          '#name'         => 'data',
          '#type'         => 'struct',
          '#description'  => t('Associative array with modified fields.'),
        ),
      ),
      '#help'     => t('Returns xml string for orders actually in given status.'),
    ),
    
    // bee_colony.orderProductsGet
    array(
      '#method'   => 'beeColony.orderProductsGet',
      '#callback' => 'xmls_bee_colony_order_products_get',  //FIXED
      '#access arguments' => array('View orders'),
      '#return'   => 'struct',
      '#file'    => array('file' => 'orders.inc', 'module' => 'bee_colony'),
      '#args'     => array(
        array(
          '#name'         => 'orders',
          '#type'         => 'struct',
          '#description'  => t('Order ID (int/array of int).'),
        ),
      ),
      '#help'     => t('Returns array for single/multiple orders.'),
    ),
    
    // bee_colony.orderProductsGet
    array(
      '#method'   => 'beeColony.orderCommentsGet',
      '#callback' => 'xmls_bee_colony_order_comments_get',  //FIXED
      '#access arguments' => array('View orders'),
      '#return'   => 'struct',
      '#file'    => array('file' => 'orders.inc', 'module' => 'bee_colony'),
      '#args'     => array(
        array(
          '#name'         => 'orders',
          '#type'         => 'struct',
          '#description'  => t('Order ID (int/array of int).'),
        ),
      ),
      '#help'     => t('Returns array for single/multiple orders.'),
    ),

    // bee_colony.orderProductsUpdate
    array(
      '#method'   => 'beeColony.orderProductsUpdate',
      '#callback' => 'xmls_bee_colony_order_products_update',   //FIXED
      '#access arguments' => array('Modify orders'),
      '#return'   => 'struct',
      '#file'    => array('file' => 'orders.inc', 'module' => 'bee_colony'),
      '#args'     => array(
        array(
          '#name'         => 'id',
          '#type'         => 'int',
          '#description'  => t('Order ID number.'),
        ),
        array(
          '#name'         => 'sku',
          '#type'         => 'int',
          '#description'  => t('Product SKU.'),
        ),
        array(
          '#name'         => 'data',
          '#type'         => 'struct',
          '#description'  => t('Associative array with modified fields.'),
        ),
      ),
      '#help'     => t('Returns associative array for modified order.'),
    ),
    
    // bee_colony.getXMLOrders
    array(
      '#method'   => 'beeColony.ordersGetXML',
      '#callback' => 'xmls_bee_colony_orders_get_xml',  //FIXED
      '#access arguments' => array('View orders'),
      '#return'   => 'string',
      '#file'    => array('file' => 'orders.inc', 'module' => 'bee_colony'),
      '#args'     => array(
        array(
          '#name'         => 'status',
          '#type'         => 'string',
          '#description'  => t('Order status.'),
        ),
      ),
      '#help'     => t('Returns xml string for orders actually in given status.'),
    ),
    
    // bee_colony.getXMLAllOrders
    array(
      '#method'   => 'beeColony.ordersGetAllXML',
      '#access arguments' => array('View orders'),
      '#callback' => 'xmls_bee_colony_orders_get_all_xml',  //FIXED
      '#return'   => 'string',
      '#file'    => array('file' => 'orders.inc', 'module' => 'bee_colony'),
      '#help'     => t('Returns xml string for orders for all statuses.'),
    ),
    
    // bee_colony.purgeOrphanTaxonomy
    array(
      '#method'   => 'beeColony.purgeOrphanTaxonomy',
      '#access arguments' => array('Purge orphan taxonomy terms'),
      '#callback' => 'xmls_bee_colony_purge_orphan_taxonomy',  //FIXED
      '#return'   => 'int',
      '#file'    => array('file' => 'products.inc', 'module' => 'bee_colony'),
      '#help'     => t('Returns Vocabulary ID or FALSE if no vocabulary catalog found.'),
    ),
    
    // bee_colony.getProduct
    array(
      '#method'   => 'beeColony.productGet',
      '#callback' => 'xmls_bee_colony_product_get',
      '#access arguments' => array('Administer catalog products'),
      '#return'   => 'struct',
      '#file'    => array('file' => 'products.inc', 'module' => 'bee_colony'),
      '#help'     => t('Get a product from Catalog by SKU.'),
      '#args'     => array(
        array(
          '#name'         => 'sku',
          '#type'         => 'int',
          '#description'  => t('Product model.'),
        ),
      ),
    ),
    
    // bee_colony.insertProduct
    array(
      '#method'   => 'beeColony.productInsert',
      '#callback' => 'xmls_bee_colony_product_insert',
      '#access arguments' => array('Administer catalog products'),
      '#return'   => 'struct',
      '#file'    => array('file' => 'products.inc', 'module' => 'bee_colony'),
      '#help'     => t('Insert a product in Catalog.'),
      '#args'     => array(
        array(
          '#name'         => 'product',
          '#type'         => 'struct',
          '#description'  => t('Product associative array data.'),
        ),
        array(
          '#name'         => 'images',
          '#type'         => 'struct',
          '#optional'     => TRUE,
          '#description'  => t('Send img files encoded base64 in structure.'),
        ),
        array(
          '#name'         => 'taxonomy',
          '#type'         => 'struct',
          '#optional'     => TRUE,
          '#description'  => t('Send taxonomy tree.'),
        ),
        array(
          '#name'         => 'force_update',
          '#type'         => 'boolean',
          '#optional'     => TRUE,
          '#description'  => t('Force update for products if already exist.'),
        ),
      ),
    ),
        
    // bee_colony.updateProduct
    array(
      '#method'   => 'beeColony.productUpdate',
      '#callback' => 'xmls_bee_colony_product_update',
      '#access arguments' => array('Administer catalog products'),
      '#return'   => 'struct',
      '#file'    => array('file' => 'products.inc', 'module' => 'bee_colony'),
      '#help'     => t('Update a product in Catalog.'),
      '#args'     => array(
        array(
          '#name'         => 'product',
          '#type'         => 'struct',
          '#description'  => t('Product associative array data.'),
        ),
        array(
          '#name'         => 'images',
          '#type'         => 'struct',
          '#optional'     => TRUE,
          '#description'  => t('Send img files encoded base64 in structure.'),
        ),
        array(
          '#name'         => 'taxonomy',
          '#type'         => 'struct',
          '#optional'     => TRUE,
          '#description'  => t('Send taxonomy tree.'),
        ),
      ),
    ),
    
    // bee_colony.deleteProduct
    array(
      '#method'   => 'beeColony.productDelete',
      '#callback' => 'xmls_bee_colony_product_delete',
      '#access arguments' => array('Administer catalog products'),
      '#return'   => 'bool',
      '#file'    => array('file' => 'products.inc', 'module' => 'bee_colony'),
      '#help'     => t('Delete a product from Catalog.'),
      '#args'     => array(
        array(
          '#name'         => 'sku',
          '#type'         => 'int',
          '#description'  => t('Product model.'),
        ),
      ),
    ),
  );
}

