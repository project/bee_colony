<?php
// $Id:$
/**
 * @file
 * BeeColony products management functions.
 */


/******************************
 * Services Callback Functions.
 ******************************/

/**
 * Get node structure for single product.
 * 
 * @param $sku
 *   String representing product model/SKU
 *   
 * @return struct
 *   Associative array that represent $node variable. This contains any
 *   information needed, but not image files.
 */

function xmls_bee_colony_product_get($sku) {
  $nid = db_result(db_query("SELECT nid FROM {uc_products} WHERE model = %d", $sku));
  $node = node_load($nid);
  return $node;
}


/**
 * Insert a new product into database.
 * 
 * @param $product
 *   Associative array containing product data. Any of following keys are
 *   considered valid:
 *   - title: Must be a string. Default is empty string.
 *   - body: Must be a string. Default is empty string.
 *   - teaser: Must be a string. Default is empty string.
 *   - model: Must be a string. It's mandatory to have this key in the array.
 *   - list_price: Must be an integer. Default is 0.
 *   - cost: Must be an integer. Default is 0.
 *   - sell_price: Must be an integer. Default is 0.
 *   - weight: Must be an integer. Default is 0.
 *   - weight_units: Must be a string. Default is Ubercart default unit.
 *   - length: Must be an integer. Default is 0.
 *   - width: Must be an integer. Default is 0.
 *   - height: Must be an integer. Default is 0.
 *   - length_units: Must be a string. Default is Ubercart default unit.
 *   - pkg_qty: Must be an integer. Default is 1.
 *   - default_qty: Must be an integer. Default is 1.
 *   - ordering: Must be an integer. Default is 0.
 *   - shippable: Must be an integer. Default is 1.
 *   - status: Product published or not. Must be 0 or 1. Default is 1 (published).
 *   Any other CCK field is supported just use field name (not label) as key.
 *   There is no type control on CCK fields. 
 *   field_image_cache is reserved for $images param, because it needs
 *   different procedures than other fields, so using it in this param gives
 *   an error.
 *
 * @param $images
 *   Array containing Associative arrays. Each array must contain structure for
 *   image files:
 *   - file: file data base64 encoded
 *   - filename: file name
 *   - filesize: file size in bytes
 *   - filemime: mime type of img
 *
 * @param $taxonomy
 *   Recursive array that represent taxonomy structure for catalog taxonomy tree
 *   
 * @param $force_update
 *   Boolean. If TRUE existant products are directly updated and no errors are
 *   shown if product already exists.
 *   
 * @return
 *   TRUE if no errors. service_errors formatted string if any error occurs.
 */
function xmls_bee_colony_product_insert($product, $images = array(), $taxonomy = array(), $force_update = FALSE) {
	global $user;
  $old = FALSE;
  $return = array( // struct data return
    'errors' => FALSE,    // Presence of errors during procedure
  );
  if (!isset($product['model'])) {
    return services_error('You must specify model number.');
  }
  if (isset($product['type'])) {
    if ($product['type'] == 'product_kit') {
      return services_error("You can't build product kits with productInsert.");
    }
    if (!uc_product_is_product($product['type'])) {
      return services_error('Product type is not ubercart product type.');
    }
  }
  $insert = new stdClass(); //Object to be used for database insertion
  $product_present = db_result(db_query("SELECT 1 FROM {uc_products} WHERE model = '%s'", $product['model']));

  // product standard fields
  $insert->title = '';
  $insert->body = '';
  $insert->teaser  ='';
  $insert->type = 'product';
  $insert->uid = $user->uid;
  $insert->model = $product['model'];
  $insert->list_price = 0;
  $insert->cost = 0;
  $insert->sell_price = 0;
  $insert->weight = 0;
  $insert->weight_units = variable_get('uc_weight_unit', 'lb');
  $insert->length = 0;
  $insert->width = 0;
  $insert->height = 0;
  $insert->length_units = variable_get('uc_length_unit', 'in');
  $insert->pkg_qty = 1;
  $insert->default_qty = 1;
  $insert->ordering = 0;
  $insert->shippable = 1;
  $insert->status = 1;

  if (!$product_present || $force_update) {   // Execute instructions only if no product found or force_update flag is true
    if ($product_present) {
      $nid = db_result(db_query("SELECT nid FROM {uc_products} WHERE model = '%s'", $product['model']));
      $type = db_result(db_query("SELECT type FROM {node} WHERE nid = %d", $nid));
      if (($type != $product['type']) && (isset($product['type']))) {
        node_delete($nid);
      } else {
        $insert = node_load($nid);
        $insert->nid = $nid;
      }
    }
    foreach ($product as $key => $value) { // Update $insert with submitted values.
      switch ($key) {
	  	case 'type':
        if (is_string($value)) {
            $insert->type = $value;
        }
        else {
          $return['errors'] = TRUE;
          $return[$key] = t('Wrong data type');
        }
		    break;

        case 'title':
          if (is_string($value)) {
            $insert->title = $value;
          }
          else {
            $return['errors'] = TRUE;
            $return[$key] = t('Wrong data type');
          }
          break;
        
        case 'body':
          if (is_string($value)) {
            $insert->body = $value;
          }
          else {
            $return['errors'] = TRUE;
            $return[$key] = t('Wrong data type');
          }
          break;
        
        case 'teaser':
          if (is_string($value)) {
            $insert->teaser = $value;
          }
          else {
            $return['errors'] = TRUE;
            $return[$key] = t('Wrong data type');
          }
          break;
        
        case 'model':
          if (is_string($value)) {
            $insert->model = $value;
          }
          else {
            $return['errors'] = TRUE;
            $return[$key] = t('Wrong data type');
          }
          break;
        
        case 'list_price':
          if (is_numeric($value)) {
            $insert->list_price = $value;
          }
          else {
            $return['errors'] = TRUE;
            $return[$key] = t('Wrong data type');
          }
          break;
        
        case 'cost':
          if (is_numeric($value)) {
            $insert->title = $value;
          }
          else {
            $return['errors'] = TRUE;
            $return[$key] = t('Wrong data type');
          }
          break;
        
        case 'sell_price':
          if (is_numeric($value)) {
            $insert->sell_price = $value;
          }
          else {
            $return['errors'] = TRUE;
            $return[$key] = t('Wrong data type');
          }
          break;
        
        case 'weight':
          if (is_numeric($value)) {
            $insert->weight = $value;
          }
          else {
            $return['errors'] = TRUE;
            $return[$key] = t('Wrong data type');
          }
          break;
          
        case 'lenght':
          if (is_numeric($value)) {
            $insert->lenght = $value;
          }
          else {
            $return['errors'] = TRUE;
            $return[$key] = t('Wrong data type');
          }
          break;
        
        case 'width':
          if (is_numeric($value)) {
            $insert->width = $value;
          }
          else {
            $return['errors'] = TRUE;
            $return[$key] = t('Wrong data type');
          }
          break;
          
        case 'height':
          if (is_numeric($value)) {
            $insert->height = $value;
          }
          else {
            $return['errors'] = TRUE;
            $return[$key] = t('Wrong data type');
          }
          break;
  
        case 'pkg_qty':
          if (is_int($value) && $value > 0) {
            $insert->pkg_qty = $value;
          }
          else {
            $return['errors'] = TRUE;
            $return[$key] = t('Wrong data type');
          }
          break;
          
        case 'default_qty':
          if (is_int($value) && $value > 0) {
            $insert->default_qty = $value;
          }
          else {
            $return['errors'] = TRUE;
            $return[$key] = t('Wrong data type');
          }
          break;
          
        case 'ordering':
          if (is_int($value)) {
            $insert->ordering = $value;
          }
          else {
            $return['errors'] = TRUE;
            $return[$key] = t('Wrong data type');
          }
          break;
          
        case 'shippable':
          if ($value === 0 || $value === 1) {
            $insert->shippable = $value;
          }
          else {
            $return['errors'] = TRUE;
            $return[$key] = t('Wrong data type. You must submit 0 or 1 as integer value.');
          }
          break;
          
        case 'status':
          if ($value === 0 || $value === 1) {
            $insert->status = $value;
          }
          else {
            $return['errors'] = TRUE;
            $return[$key] = t('Wrong data type. You must submit 0 or 1 as integer value.');
          }
          break;
          
        case 'field_image_cache':
          $return['errors'] = TRUE;
          $return[$key] = t('You must submit images through image parameter.');
          break;
        
        case 'taxonomy':
          $return['errors'] = TRUE;
          $return[$key] = t('You must submit categories through taxonomy parameter.');
          break;
        
          
        default:
          if (content_fields($key, 'product')) { // Need to improve this by using cck hooks or similar
            $insert->$key = $value;
          }
          else {
            $return['errors'] = TRUE;
            $return[$key] = t('Field does not exist.');
          }
          break;
      }
    } // End of $insert updating with submitted values (foreach cycle)

    // Start of taxonomy update for product.
    $vocabulary = variable_get('uc_catalog_vid', FALSE);
    
    if ($vocabulary && !empty($taxonomy)) {  // Check for catalog existence and Taxonomy structure param.
        $result_taxo = bee_colony_taxonomy_tree($taxonomy, 0, 0, $vocabulary);
        if ($result_taxo) {
          $insert->taxonomy = $result_taxo;
        }
        else {
          $return['errors'] = TRUE;
          //$return['taxonomy'] = t('Taxonomy data error.');
          return services_error('Taxonomy data error.');
        }
    } //End of taxonomy update for product.
    $field = content_fields('field_image_cache', 'product');
    if (!empty($images) && $field) {
      // product images support;
      if ($insert->field_image_cache[0] != NULL) {  //If any images present delete it
        foreach ($insert->field_image_cache as $old_image) {
          imagefield_file_delete($old_image);
        }
        unset($insert->field_image_cache);
      }
      
      foreach ($images as $img) {
        $img = (object) $img;
        $img->filepath = bee_colony_file_path($field).'/'. $img->filename;
        $tmp = bee_colony_img_save($img);
        $insert->field_image_cache[] = (array) $tmp;
      }
    }
    
    if (!$return['errors']) {  
      node_save($insert);
    }
  }  // End if statement for already existant product or force_update instruction
  else {
    $return['errors'] = TRUE;
    return services_error('Product already existant');
  }
  
  if (!$return['errors']) return TRUE;
  else return services_error('Errors found. Serialized error array = '. serialize($return));
}



/**
 * Updates a single product, if exist.
 * 
 * @param $product
 *   Array containing keys and values to be updated. Same keys as 
 *   xmls_bee_colony_product_insert function are considered valid.
 *   
 * @param $images
 *   Array containing product images structure. Same structure as 
 *   xmls_bee_colony_product_insert is considered valid.
 * 
 * @param $taxonomy
 *   Array that represent catalog structure for product. Same structure as
 *   xmls_bee_colony_product_insert is considered valid.
 *   
 * @return bool
 *   TRUE if no errors.
 */
function xmls_bee_colony_product_update($product, $images = array(), $taxonomy = array()) {
  $product_present = db_result(db_query("SELECT 1 FROM {uc_products} WHERE model = %d", $sku)); // Check for product
  if ($product_present) {   // Execute instructions only if product found
    return xmls_bee_colony_product_insert($product, $images, $taxonomy, TRUE);
  }
  else {
    return services_error("Product does not exists.");
  }
}

/**
 * Delete a single product.
 * 
 * @param $sku
 *   String that represent product SKU
 *   
 * @return boolean
 *   TRUE if no errors.
 */
function xmls_bee_colony_product_delete($sku) {
  $nid = db_result(db_query("SELECT nid FROM {uc_products} WHERE model = %d", $sku)); // Check for product
  if ($nid) {   // Execute instructions only if product found
    node_delete($nid);
    return TRUE;
  }
  else {
    return services_error("Product does not exists.");
  }
}





/*******************************
 * Internal procedure functions.
 *******************************/

/**
 * Check and updates Catalog taxonomy tree.
 * 
 * @param $taxonomy
 * @param $parent
 * @param $depth
 * @param $vid
 * @return unknown_type
 */
function bee_colony_taxonomy_tree($taxonomy, $parent, $depth, $vid) {
  $return = array();
  $newterm = TRUE;
  
  $check = bee_colony_taxonomy_array_check($taxonomy);
  if ($check != FALSE) return FALSE;
  
  foreach ($taxonomy as $passed_term) {  // Analyze each term passed in taxonomy array
    $newterm = TRUE;
    
    if (!isset($passed_term['name']) || empty($passed_term['name'])) return FALSE; // Check if wrong structure return false
    
    $tmp_term_by_name = taxonomy_get_term_by_name($passed_term['name']);  //TRY FIND TERMS WITH SAME NAME
    
    // START SEARCH FOR EXISTING TERM, ONLY IF TERM WITH SAME NAME EXIST
    if (!empty($tmp_term_by_name)) { // TERM NAME FOUND. N.B. $tmp_term_by_name, if present, is an array containing stdClass objects. Return tested as true on found term.
      foreach ($tmp_term_by_name as $term) { // Analyze each term found by name.
        if (!$newterm) break; //If term is not new (term has been found in previous iterations) stop searching
        
         //TODO: INSERT HERE CHECK FOR DATA INTEGRITY
        $tmp_parent = taxonomy_get_parents($term->tid);  // A "single" term is checked for parents. N.B. $term is stdClass object. $tmp_parent is an array containing stdClass objects.
        
        if (($depth == 0) && empty($tmp_parent)) {  // Term is identified as root term. (depth = 0 and no parents)
          $tid = $term->tid;  
          $return[$term->tid] = bee_colony_populate_node_term($term);
          $newterm = FALSE; //Term identified as existing root term
          bee_colony_update_term($passed_term, $term->tid, $parent);
        }
        elseif (($depth > 0)) { // Term is not root (depth > 0) and have one or more parents. 
          foreach ($tmp_parent as $single_parent) { // Analyze each parent found, N.B. if no parents found this cycle is automatically skipped.
            if ($single_parent->tid == $parent) { // If we found same parent.
              $return[$term->tid] = bee_colony_populate_node_term($term);
              $tid = $term->tid;
              $newterm = FALSE; //Term identified as existing non-root term
              bee_colony_update_term($passed_term, $term->tid, $parent);
            }
          } // End foreach (Analyze parents)
        } // End elseif (term not root)
      } // End foreach (terms found by name)
    }  // End case of term name found
    // END CHECK OF TERM IF TERM WITH SAME NAME EXIST
    
    // We have two cases now:
    // 1- We are analyzing a root term (depth = 0), but parent terms found (empty($tmp_parent)) some lines ago. (not possible :) )
    // 2- Not root term (depth > 0), but no terms with same parent found
    // Anyway it's a new term in our taxonomy. Here we build it, taxonomy_save_term takes array as argument.
    if ($newterm) {  // If term has not been found our $newterm statement is true, obviously it's a new term. Here we build it, taxonomy_save_term() takes array as argument.
      $new = array(
        'name' => $passed_term['name'],
        'description' => $passed_term['description'],
        'vid' => $vid,
        'weight' => $passed_term['weight'],
        'parent' => $parent
      );
      taxonomy_save_term($new);
      $tid = $new['tid'];
      $return[$new['tid']] = bee_colony_populate_node_term($new);
    }
    
    if (isset($passed_term['img'])) {  // Term image support
      bee_colony_taxonomy_image($passed_term['img'], $tid);
    }
    
    if (isset($passed_term['child'])) { // Recursive function call when child term is found.
      $child = bee_colony_taxonomy_tree($passed_term['child'], $tid, $depth+1, $vid);
      if ($child == FALSE) return FALSE; // If errors in child errors
      else {
        $return = $return + $child;
      }
    }
  } // End foreach cycle (taxonomy passed array)
  return $return;
}

/**
 * Update an existing term.
 * 
 * Updates term identified by $tid with data from $passed_term (only weight and description).
 * @param struct $passed_term
 * @param int $tid
 */
function bee_colony_update_term($passed_term, $tid, $parent) {
  $term = taxonomy_get_term($tid, TRUE);
  $update = array(
    'name' => $term->name,
    'description' => $passed_term['description'],
    'vid' => $term->vid,
    'weight' => $passed_term['weight'],
    'tid' => $term->tid,
    'parent' => $parent
  );
  taxonomy_save_term($update);
}

/**
 * Purge orphan taxonomy terms.
 */
function xmls_bee_colony_purge_orphan_taxonomy() {
	$vid = variable_get('uc_catalog_vid', FALSE);
	if ($vid === FALSE) return FALSE;
  $terms = taxonomy_get_tree($vid);
  foreach ($terms as $term) {
    if (taxonomy_term_count_nodes($term->tid) <= 0) {
      taxonomy_del_term($term->tid);
    }
  }
  return $vid;
}

/**
 * Check data in array structure
 * 
 * @param $taxonomy
 *   Array to be checked
 *   
 * @param $depth
 *   Depth of term checked
 *   
 * @return
 *   
 */
function bee_colony_taxonomy_array_check($taxonomy, $depth = 0) {
  $return = FALSE;
  $array_taxonomy = (array) $taxonomy;
  foreach ($array_taxonomy as $term) {
    foreach ($term as $key => $value) {
      switch ($key) {
        case 'name':
          if (!is_string($value))
          $return['term'] = array(
            'key' => $key,
            'value' => $value,
            'depth' => $depth,
          );
          break;
        
        case 'weight':
          if (!is_int($value))
          $return['term'] = array(
            'key' => $key,
            'value' => $value,
            'depth' => $depth,
          );
          break;
        
        case 'description':
          if (!is_string($value))
          $return['term'] = array(
            'key' => $key,
            'value' => $value,
            'depth' => $depth,
          );
          break;
          
        case 'img':
          //check img data
          break;
          
        case 'child':
          $tmp = bee_colony_taxonomy_array_check($taxonomy['child'], $depth+1);
          if ($tmp != FALSE) $return['child'] = $tmp;
          break;
        
        default:
          $return[$key] = 'Unknown key';
          break;
      }
    }
  }
  return $return;
}

/**
 * Populate term structure to insert in node.
 * 
 * @param $term
 *   $term structure 
 * @return unknown_type
 */
function bee_colony_populate_node_term($term) {
  $term = (array) $term;
  $termnew = new stdClass();
  $termnew->tid = $term['tid'];
  $termnew->vid = $term['vid'];
  $termnew->name = $term['name'];
  $termnew->description = $term['description'];
  $termnew->weight = $term['weight'];
  return $termnew;
}

/**
 * Save file information.
 *
 * This is basically a trimmed copy of file_save_upload() from includes/file.inc
 *
 * @param $file
 *   Array representing the file information
 */
function bee_colony_img_save($file) {
	global $user;
  // Build the list of non-munged extensions.
  $extensions = 'jpg jpeg gif png';

  // Get the directory name for the location of the file:
  $dir = dirname($file->filepath);
  // Build the destination folder tree if it doesn't already exists.
  if (!file_check_directory($dir, FILE_CREATE_DIRECTORY)) {
    return services_error("Could not create destination directory for file.");
  }

  // Update file object as necessary
  $file->filepath = file_destination(file_create_path($file->filepath), FILE_EXISTS_RENAME);
  $file->filename = file_munge_filename(trim(basename($file->filepath), '.'), $extensions, TRUE);
  $file->filemime = file_get_mimetype($file->filename);
  $file->uid = $user->uid;
  $file->timestamp = time();

  // Rename potentially executable files, to help prevent exploits.
  if (preg_match('/\.(php|pl|py|cgi|asp|js)$/i', $file->filename) && (substr($file->filename, -4) != '.txt')) {
    $file->filemime = 'text/plain';
    $file->filepath .= '.txt';
    $file->filename .= '.txt';
  }

  // If the destination is not provided, or is not writable, error our
  if (empty($file->filepath) || file_check_path($file->filepath) === FALSE) {
    return services_error("Destination directory does not exist or is not writeable.");
  }

  //The filepath that ends up in the node must contain the filename
  $file->filepath .= '/'. $file->filename;

  // Write the file
  if (!file_save_data(base64_decode($file->file), $file->filepath)) {
    return services_error("Could not write file to destination");
  }

  // If we made it this far it's safe to record this file in the database.
  drupal_write_record('files', $file);
  file_set_status($file, FILE_STATUS_PERMANENT);
  
  // hook_file_insert() requires an object
    foreach (module_implements('file_insert') as $module) {
      $function =  $module .'_file_insert';
      $function($file);
    }
  //Add or refresh file to cache.
  _field_file_cache($file);
  // Return the file
  return $file;
}

/**
 * Check and updates image for catalog term.
 * 
 * @param $img
 *   Array that represent image file.
 *   
 * @param $tid
 *   Integer that represent taxonomy term ID
 */
function bee_colony_taxonomy_image($img, $tid) {
  $query = db_query("SELECT * FROM {uc_catalog_images} WHERE tid = %d", $tid);
  $img = (object) $img;
  //INSERT HERE CHECK FOR FILE STRUCTURE
  
  if ($result = db_fetch_object($query)) { // If term already have img
    if ($result->filesize != $img->filesize) { // Check filesize
      db_query("DELETE FROM {uc_catalog_images} WHERE fid = %d ", $result->fid);  // If a different image (different size) is present then update it.
      db_query("DELETE FROM {files} WHERE fid = %d ", $result->fid);
      $img->filepath = file_directory_path().'/'. $img->filename;
      $img = bee_colony_img_save($img);
      db_query("INSERT INTO {uc_catalog_images} (fid, tid, filename, filepath, filemime, filesize) VALUES (%d, %d, '%s', '%s', '%s', %d)", $img->fid, $tid, $img->filename, $img->filepath, $img->filemime, $img->filesize);
    }
  }
}