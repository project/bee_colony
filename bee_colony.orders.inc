<?php
// $Id:$
/**
 * @file
 * BeeColony orders management functions.
 */


/******************************
 * Services Callback Functions.
 ******************************/

/**
 * Get ubercart orders for status.
 * 
 * Get all ubercart orders from database for a single status
 * and return them as an associative array.
 * 
 * @param $status
 *   String that is equal to ubercart order status
 *   
 * @param $ext
 *   Bool 
 *   
 * @return unknown_type
 *   Associative array with orders data.
 */
function xmls_bee_colony_orders_get_by_status($status, $ext = TRUE) { //$ext control if function is called directly from XML-RPC or if it's a subroutine of get_all_orders 
  $orders = array();
  $status_query = db_query("SELECT * FROM {uc_order_statuses} WHERE title = '%s'", $status);  //Pick up from database status id correspondant to status string requested.
  while ($row_status = db_fetch_array($status_query)) {
    if ($ext) $orders['status'] = $row_status['title'];
    $order_query = db_query("SELECT * FROM {uc_orders} WHERE order_status = '%s'", $row_status['order_status_id']);  //Pick up from database orders in requested status.
    while ($row_order = db_fetch_array($order_query)) {  //Building of object
      $orders[$row_order['order_id']] = $row_order;
      
      // unserialize array for VAT and VAT2
      $orders[$row_order['order_id']]['data'] = unserialize($row_order['data']);
      
      // convert numeric contry and zone to string
      $dcountry = db_result(db_query("SELECT country_name FROM {uc_countries} WHERE country_id = %d", $row_order['delivery_country']));
      $dzone = db_result(db_query("SELECT zone_name FROM {uc_zones} WHERE zone_country_id = %d AND zone_id = %d", $row_order['delivery_country'], $row_order['delivery_zone']));
      $orders[$row_order['order_id']]['delivery_country'] = $dcountry;
      $orders[$row_order['order_id']]['delivery_zone'] = $dzone;
      $bcountry = db_result(db_query("SELECT country_name FROM {uc_countries} WHERE country_id = %d", $row_order['billing_country']));
      $bzone = db_result(db_query("SELECT zone_name FROM {uc_zones} WHERE zone_country_id = %d AND zone_id = %d", $row_order['billing_country'], $row_order['billing_zone']));
      $orders[$row_order['order_id']]['billing_country'] = $bcountry;
      $orders[$row_order['order_id']]['billing_zone'] = $bzone;
    }
  }
  if (($status_query !== FALSE) && (empty($orders))) return services_error('No orders for status.');  //if no status id were found and query is succesful this is the only instruction executed.
  if ($status_query === FALSE) return services_error('Status not available.');
  return $orders;
}

/**
 * Get all ubercart orders.
 * 
 * Get all ubercart orders from database and return them as an associative array.
 * 
 * @return struct
 *   Associative array with orders data ordered by order status.
 */
function xmls_bee_colony_orders_get_all() {
  $query = db_query("SELECT * FROM {uc_order_statuses}");
  
  $orders = array();
  while ($row = db_fetch_array($query)) {
    $orders[$row['title']] = xmls_bee_colony_orders_get_by_status($row['title'], FALSE);
  }
  return $orders;
}

/**
 * Modify order.
 * 
 * Modify basic data for an order.
 * 
 * @param int $id
 *   Order ID number. Assumed as integer value.
 *   
 * @param struct $data
 *   An associative array containing values to be modified. Any of following
 *   keys are considered valid:
 *   - order_status: A string to use for localized order status. It's part of
 *     Ubercart configuration.
 *   - delivery_first_name: A string to use for First Name in delivery address.
 *   - delivery_last_name: A string to use for Last Name in delivery address.
 *   - delivery_phone: A string to use for Phone Number in delivery address.
 *   - delivery_company: A string to use for Company in delivery address
 *   - delivery_street1: A string to use for first line in delivery address.
 *   - delivery_street2: A string to use for last line in delivery address.
 *   - delivery_city: A string to use for City in delivery address.
 *   - delivery_zone: A string to use for Zone in delivery address. Ubercart has
 *     its own zone list, string is checked against list stored in database. 
 *   - delivery_postal_code: A string to use for Postal Code in delivery address.
 *   - delivery_country: A string to use for Country in delivery address.
 *     Ubercart has its own country list, string is checked against list
 *     stored in database.
 *   - billing_first_name: A string to use for First Name in billing address.
 *   - billing_last_name: A string to use for Last Name in billing address.
 *   - billing_phone: A string to use for Phone Number in billing address.
 *   - billing_company: A string to use for Company in billing address
 *   - billing_street1: A string to use for first line in billing address.
 *   - billing_street2: A string to use for last line in billing address.
 *   - billing_city: A string to use for City in billing address.
 *   - billing_zone: A string to use for Zone in billing address. Ubercart
 *     has its own zone list, string is checked versus list stored in database. 
 *   - billing_postal_code: A string to use for Postal Code in billing address.
 *   - billing_country: A string to use for Country in billing address.
 *     Ubercart has its own country list, string is checked against list
 *     stored in database.
 *   - payment_method: A string to use for Payment Method.
 *   - data: Associative array containing various data like VAT numbers.
 *   
 * @return string/boolean
 *   Different return values are possible:
 *   - TRUE: If database has been succesfully updated.
 *   - Error Message: If any error has occurred.
 */
function xmls_bee_colony_order_update($id, $data) {
  $update = new stdClass();
  $error = array();
  $status = FALSE;
  $exist = db_result(db_query("SELECT 1 FROM {uc_orders} WHERE order_id = %d", $id));  //Check for order presence
  if ($exist) {
    $update->order_id = $id;
    foreach ($data as $key => $value) {  // build object with values to update.
      switch ($key) {
        case 'order_status':
          $status = db_result(db_query("SELECT order_status_id FROM {uc_order_statuses} WHERE title = '%s'", $value));
          if (!$status) {
            $error[$key] = $value;
          }
          break;
        case 'delivery_first_name':
          if (is_string($value)) {
            $update->delivery_first_name = $value;
          }
          else {
            $error[$key] = $value;
          }
          break;      
        case 'delivery_last_name':
          if (is_string($value)) {
            $update->delivery_last_name = $value; 
          }
          else {
            $error[$key] = $value;
          }
          break;      
        case 'delivery_phone':
           if (is_string($value)) {
            $update->delivery_phone = $value;
           }
           else {
            $error[$key] = $value;
          }
          break;      
        case 'delivery_company':
          if (is_string($value)) {
            $update->delivery_company = $value;
          }
          else {
            $error[$key] = $value;
          }
          break;      
        case 'delivery_street1':
          if (is_string($value)) {
            $update->delivery_street1 = $value;
          }
          else {
            $error[$key] = $value;
          }
          break;      
        case 'delivery_street2':
          if (is_string($value)) {
            $update->delivery_street2 = $value;
          }
          else {
            $error[$key] = $value;
          }
          break;      
        case 'delivery_city':
          if (is_string($value)) {
            $update->delivery_city = $value;
          }
          else {
            $error[$key] = $value;
          }
          break;      
        case 'delivery_zone':
          $zone = db_result(db_query("SELECT zone_id FROM {uc_zones} WHERE zone_name = '%s'", $value));
          if ($zone) {
            $update->delivery_zone = $zone;
          }
          else {
            $error[$key] = $value;
          }
          $update->delivery_zone = $value;
          break;      
        case 'delivery_postal_code':
          if (is_string($value)) {
            $update->delivery_postal_code = $value;
          }
          else {
            $error[$key] = $value;
          }
          break;      
        case 'delivery_country':
          $country = db_result(db_query("SELECT country_id FROM {uc_countries} WHERE country_name = '%s'", $value));
          if ($country) {
            $update->delivery_country = $country;
          }
          else {
            $error[$key] = $value;
          }
          $update->delivery_country = $value;
          break;      
        case 'billing_first_name':
          if (is_string($value)) {
            $update->billing_first_name = $value;
          }
          else {
            $error[$key] = $value;
          }
          break;      
        case 'billing_last_name':
          if (is_string($value)) {
            $update->billing_last_name = $value;
          }
          else {
            $error[$key] = $value;
          }
          break;      
        case 'billing_phone':
          if (is_string($value)) {
            $update->billing_phone = $value;
          }
          else {
            $error[$key] = $value;
          }
          break;      
        case 'billing_company':
          if (is_string($value)) {
            $update->billing_company = $value;
          }
          else {
            $error[$key] = $value;
          }
          break;      
        case 'billing_street1':
          if (is_string($value)) {
            $update->billing_street1 = $value;
          }
          else {
            $error[$key] = $value;
          }
          break;      
        case 'billing_street2':
          if (is_string($value)) {
            $update->billing_street2 = $value;
          }
          else {
            $error[$key] = $value;
          }
          break;      
        case 'billing_city':
          if (is_string($value)) {
            $update->billing_city = $value;
          }
          else {
            $error[$key] = $value;
          }
          break;      
        case 'billing_zone':
          $zone = db_result(db_query("SELECT zone_id FROM {uc_zones} WHERE zone_name = '%s'", $value));
          if ($zone) {
            $update->billing_zone = $zone;
          }
          else {
            $error[$key] = $value;
          }
          $update->billing_zone = $value;
          break;      
        case 'billing_postal_code':
          if (is_string($value)) {
            $update->billing_postal_code = $value;
          }
          else {
            $error[$key] = $value;
          }
          break;      
        case 'billing_country':
          $country = db_result(db_query("SELECT country_id FROM {uc_countries} WHERE country_name = '%s'", $value));
          if ($country) {
            $update->billing_country = $country;
          }
          else {
            $error[$key] = $value;
          }
          $update->billing_country = $value;
          break;      
        case 'payment_method':
          if (is_string($value)) {
            $update->payment_method = $value;
          }
          else {
            $error[$key] = $value;
          }
          break;
        case 'data':
          if (is_array($value)) {
            $update->data=serialize($value);
          }
          else {
            $error[$key] = $value;
          }
          break;
        default:
          $error[$key] = 'Unknown Key';
          break;      
      }
    }
    if (empty($error)) {
      //$status = db_result(db_query("SELECT order_status_id FROM {uc_order_statuses} WHERE title = '%s'", $value));
      if ($status) {
        uc_order_update_status($id, $status);
      }
      $update->modified = time();
      $return = drupal_write_record('uc_orders', $update, 'order_id'); 
      if ($return != SAVED_UPDATED) return services_error('An error has occurred during database update. Please contact system administrator.');
      else return TRUE;
    }
    else {
      $str = 'Following Keys and Values are not valid: Serialized errors array='. serialize($error);
      return services_error($str);
    }
  }
  else {
    return services_error('Order ID:'. $id .' does not exists.');
  }
}

/**
 * Get product list for orders.
 * 
 * Get products for single or multiple orders.
 * 
 * @param struct $order_id
 *   Order ID value. Assumed as integer value or array of integer values.
 * 
 * @return struct
 *   Associative array with product ordered data.
 */
function xmls_bee_colony_order_products_get($order_id) { 
  if (is_int($order_id)) {
    $return[$order_id] = bee_colony_fetch_products($order_id);
  }
  elseif (is_array($order_id)) {
    foreach ($order_id as $id) {
      if (is_int($id)) {
        $return[$id] = bee_colony_fetch_products($id);
      }
      else {
        return services_error(t('Invalid data type for id @id, you must submit array built with integers.', array( '@id' => $id)));
      }
    }
  }
  else {
    return services_error('Invalid data type, you must submit integer or array built with integers.');
  }
  return $return;
}

/**
 * Get comments for orders.
 * 
 * Get comments for single or multiple orders.
 * 
 * @param struct $order_id
 *   Order ID value. Assumed as integer value or array of integer values.
 * 
 * @return struct
 *   Associative array with product ordered data.
 */
function xmls_bee_colony_order_comments_get($order_id) { 
  if (is_int($order_id)) {
    $return[$order_id] = bee_colony_fetch_comments($order_id);
  }
  elseif (is_array($order_id)) {
    foreach ($order_id as $id) {
      if (is_int($id)) {
        $return[$id] = bee_colony_fetch_comments($id);
      }
      else {
        return services_error(t('Invalid data type for id @id, you must submit array built with integers.', array( '@id' => $id)));
      }
    }
  }
  else {
    return services_error('Invalid data type, you must submit integer or array built with integers.');
  }
  return $return;
}
/**
 * Modify order products.
 * 
 * Modify model or quantity for any product in order.
 * 
 * @param int $id
 *   Order ID number. Assumed as integer value.
 *   
 * @param string $sku
 *   Product model. String value.
 * 
 * @param struct $data
 *   An associative array containing values to be modified. Any of following keys
 *   is considered valid:
 *   - qty: New quantity for product.
 *   - model: New model for product, all other product data are retriebved from database.
 */
function xmls_bee_colony_order_products_update($order_id, $sku, $data) {
  $product_update = new stdClass();
  $order_update = new stdClass();
  $error = array();
  $product = db_fetch_object(db_query("SELECT * FROM {uc_order_products} WHERE order_id = %d AND model = %d", $order_id, $sku));  //Check for order product presence
  if ($product) {
    $product_update->order_id = $order_id;
    $order_update->order_id = $order_id;
    $product_update->model = $sku;
    $product_update->order_product_id = $product->order_product_id;
    foreach ($data as $key => $value) {
      switch ($key) {
        case 'qty':
          if (is_int($value) && $value > 0) {
            $product_update->qty = $value;
          }
          else {
            $error['qty'] = $value;
          }
          break;
        case 'model':
          $uc_product = db_fetch_object(db_query("SELECT * FROM {uc_products} WHERE model = %d", $value));
          if ($uc_product) {
            $product_update->nid = $uc_product->nid;
            $node_product = db_fetch_object(db_query("SELECT * FROM {node} WHERE nid = %d", $uc_product->nid));
            $product_update->title = $node_product->title;
            $product_update->model = $value;
            $product_update->cost = $uc_product->cost;
            $product_update->price = $uc_product->sell_price;
            $product_update->weight = $uc_product->weight;
            
          }
          else {
            $error['model'] = $value;
          }
          break;
        default:
          $error[$key] = FALSE;
          break;    
      }
    }
    if (empty($error)) {
      $return = array();
      $return['order_update'] = $order_update;
      $return['product_update'] = $product_update;
      $order_update->modified = time();
      $return['products'] = drupal_write_record('uc_order_products', $product_update, 'order_product_id');
      $order_update->order_total = bee_colony_update_total($order_id);
      $return['order'] = drupal_write_record('uc_orders', $order_update, 'order_id');
      return TRUE; 
    }
    else {
      $str = 'Following Keys and Values are not valid: Serialized errors array='. serialize($error);
      return services_error($str);
    }
  }
  else {
    return services_error('Product or order do not exist.');
  }
}

/**
 * Get all ubercart orders XML.
 * 
 * Get all ubercart orders from database and return them as XML structure.
 * 
 * @return string
 *   String with orders data ordered by order status.
 */
function xmls_bee_colony_orders_get_all_xml() {
  $query = db_query("SELECT * FROM {uc_order_statuses}");
  
  $orders = '';
  $orders .= t('<allOrders>');
  while ($row = db_fetch_object($query)) {
    $orders .= xmls_bee_colony_get_xml_orders($row->title);
  }
  $orders .=t('</allOrders>');
  return $orders;
}

/**
 * Get ubercart orders for status.
 * 
 * Get all ubercart orders from database for a single status
 * and return them as an XML structure.
 * 
 * @return string
 *   String with orders data.
 */
function xmls_bee_colony_orders_get_xml($status, $ext = FALSE) {
  $orders = '';
  $status_query = db_query("SELECT * FROM {uc_order_statuses} WHERE title = '%s'", $status);  //Pick up from database status id correspondant to status string requested.
  while ($row_status = db_fetch_object($status_query)) {
    $row_status->title = str_replace(' ', '_', $row_status->title);  //Remove spaces char from status due to XML Syntax rules.
    $orders .= t('<!status>', array('!status' => $row_status->title) );
    $order_query = db_query("SELECT * FROM {uc_orders} WHERE order_status = '%s'", $row_status->order_status_id);  //Pick up from database orders in requested status.
    while ($row_order = db_fetch_object($order_query)) {  //Building of XML Struct
      $orders .= t('<order>');
      $orders .= t('<orderID>!orderID</orderID>', array('!orderID' => $row_order->order_id));
      $orders .= t('<userID>!userID</userID>', array('!userID' => $row_order->uid));
      $orders .= t('<primaryEmail>!email</primaryEmail>', array('!email' => $row_order->primary_email));
      $orders .= t('<deliveryAddress>');
        $orders .= t('<deliveryFirstName>!firstname</deliveryFirstName>', array('!firstname' => $row_order->delivery_first_name));
        $orders .= t('<deliveryLastName>!lastname</deliveryLastName>', array('!lastname' => $row_order->delivery_last_name));
        $orders .= t('<deliveryCompany>!company</deliveryCompany>', array('!company' => $row_order->delivery_company));
        $orders .= t('<deliveryPhone>!phone</deliveryPhone>', array('!phone' => $row_order->delivery_phone));
        $orders .= t('<deliveryStreet1>!street1</deliveryStreet1>', array('!street1' => $row_order->delivery_street1));
        $orders .= t('<deliveryStreet2>!street2</deliveryStreet2>', array('!street2' => $row_order->delivery_street2));
        $orders .= t('<deliveryCity>!city</deliveryCity>', array('!city' => $row_order->delivery_city));
        $dcountry = db_result(db_query("SELECT country_name FROM {uc_countries} WHERE country_id = %d", $row_order->delivery_country));
        $dzone = db_result(db_query("SELECT zone_name FROM {uc_zones} WHERE zone_country_id = %d AND zone_id = %d", $row_order->delivery_country, $row_order->delivery_zone));
        $orders .= t('<deliveryZone>!zone</deliveryZone>', array('!zone' => $dzone));
        $orders .= t('<deliveryCountry>!country</deliveryCountry>', array('!country' => $dcountry));
        $orders .= t('<deliveryPostalCode>!pcode</deliveryPostalCode>', array('!pcode' => $row_order->delivery_postal_code));
      $orders .= t('</deliveryAddress>');
      $orders .= t('<billingAddress>');
        $orders .= t('<billingFirstName>!firstname</billingFirstName>', array('!firstname' => $row_order->billing_first_name));
        $orders .= t('<billingLastName>!lastname</billingLastName>', array('!lastname' => $row_order->billing_last_name));
        $orders .= t('<billingCompany>!company</billingCompany>', array('!company' => $row_order->billing_company));
        $orders .= t('<billingPhone>!phone</billingPhone>', array('!phone' => $row_order->billing_phone));
        $orders .= t('<billingStreet1>!street1</billingStreet1>', array('!street1' => $row_order->billing_street1));
        $orders .= t('<billingStreet2>!street2</billingStreet2>', array('!street2' => $row_order->billing_street2));
        $orders .= t('<billingCity>!city</billingCity>', array('!city' => $row_order->billing_city));
        $bcountry = db_result(db_query("SELECT country_name FROM {uc_countries} WHERE country_id = %d", $row_order->billing_country));
        $bzone = db_result(db_query("SELECT zone_name FROM {uc_zones} WHERE zone_country_id = %d AND zone_id = %d", $row_order->billing_country, $row_order->billing_zone));
        $orders .= t('<billingZone>!zone</billingZone>', array('!zone' => $bzone));
        $orders .= t('<billingCountry>!country</billingCountry>', array('!country' => $bcountry));
        $orders .= t('<billingPostalCode>!pcode</billingPostalCode>', array('!pcode' => $row_order->billing_postal_code));
      $orders .= t('</billingAddress>');
      $tmp_vat = unserialize($row_order->data);
      $orders .= t('<vatNumber>!vat</vatNumber>', array('!vat' => $tmp_vat['vat_number']));
      $orders .= t('<vatNumber2>!vat2</vatNumber2>', array('!vat2' => $tmp_vat['vat_number2']));
      $orders .= t('<paymentMethod>!pmeth</paymentMethod>', array('!pmeth' => $row_order->payment_method));
      $orders .= t('<totalPrice>!total</totalPrice>', array('!total' => $row_order->order_total));
      $orders .= t('<products>');
      $product_query = db_query("SELECT * FROM {uc_order_products} WHERE order_id = %d", $row_order->order_id);
      while ($row_product = db_fetch_object($product_query)) {
        $orders .= t('<product>');
          $orders .= t('<productID>!pid</productID>', array('!pid' => $row_product->model));
          $orders .= t('<productTitle>!ptitle</productTitle>', array('!ptitle' => $row_product->title));
          $orders .= t('<productQty>!pqty</productQty>', array('!pqty' => $row_product->qty));
          $orders .= t('<productPrice>!pprice</productPrice>', array('!pprice' => $row_product->price));
        $orders .= t('</product>');
      }
      $orders .= t('</products>');
      $orders .= t('</order>');
    }
    $orders .= t('</!status>', array('!status' => $row_status->title) );
  }
  if (($orders == '') && ($status_query !== FALSE)) return services_error('Status not available.');  //if no status id were found (stQuery = FALSE) this is the only instruction executed. 
  return $orders;
}





/*******************************
 * Internal procedure functions.
 *******************************/

/**
 * Calculate order order total price.
 * 
 * Counts all product in an order and calculate total order price.
 * 
 * @param int $order_id
 *   Order ID value. Assumed as integer value.
 *   
 * @return decimal
 *   Numeric value equal to order total price.
 */
function bee_colony_update_total($order_id) {
  $total= 0;
  
  $products_query = db_query("SELECT * FROM {uc_order_products} WHERE order_id = %d", $order_id);
    while ($row = db_fetch_object($products_query)) {
      $tmp = $row->price * $row->qty;
      $total += $tmp;
    }
    return $total;

}

/**
 * Get product list for single order.
 * 
 * @param int $order_id
 *   Order ID value. Assumed as integer value.
 * 
 * @return struct
 *   Associative array with product ordered data.
 */
function bee_colony_fetch_products($order_id) {
  $return = array();
  $product_query = db_query("SELECT * FROM {uc_order_products} WHERE order_id = %d", $order_id);
  while ($row_product = db_fetch_array($product_query)) {
    $return[$row_product['model']] = $row_product;
    $return[$row_product['model']]['data'] = unserialize($row_product['data']);
  }
  if (($product_query !== FALSE) && (empty($return))) $return = t('No products for order_id !order_id', array('!order_id' => $order_id)); //Check if $return contain no data and query was succesful (correct syntax)
  return $return;
}

/**
 * Get comment list for single order.
 * 
 * @param int $order_id
 *   Order ID value. Assumed as integer value.
 * 
 * @return struct
 *   Associative array with product ordered data.
 */
function bee_colony_fetch_comments($order_id) {
  $return = array();
  $comments_query = db_query("SELECT * FROM {uc_order_comments} WHERE order_id = %d", $order_id);
  while ($row_comments = db_fetch_array($comments_query)) {
    $return['user_comments'][$row_comments['comment_id']] = $row_comments['message'];
  }
  $admin_query = db_query("SELECT * FROM {uc_order_admin_comments} WHERE order_id = %d", $order_id);
  while ($row_comments = db_fetch_array($admin_query)) {
    $return['admin_comments'][$row_comments['comment_id']] = $row_comments['message'];
  }
  if (($admin_query !== FALSE) && ($comments_query !== FALSE) && (empty($return))) $return = t('No comments for order_id !order_id', array('!order_id' => $order_id)); //Check if $return contain no data and query was succesful (correct syntax)
  return $return;
}

