$Id:$

-- SUMMARY --

Set of methods utilizzable with services API to interact with Ubercart order
and products.

For a full description of the module, visit the project page:
  http://drupal.org/project/bee_colony

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/bee_colony


-- REQUIREMENTS --

- Ubercart module, version 2.x
- Services Module, version 2.x


-- INSTALLATION --

* Install as usual, see INSTALL.txt for further information.


-- CONFIGURATION --

* It is "STRONGLY RECCOMENDED"  to setup Session ID and API KEY authentication
(or any other authentication module) inside page
Administer >> Site Building >> Services >> Settings

-- SERVICES INFO --
* Actually 11 services have been implemented:
  - beeColony.ordersGetByStatus(status)
  - beeColony.ordersGetAll()
  - beeColony.orderUpdate(id, data)
  - beeColony.orderProductsGet(orders)
  - beeColony.orderProductsUpdate(id, sku, data)
  - beeColony.ordersGetXML(status)
  - beeColony.ordersGetAllXML()
  - beeColony.productGet(sku)
  - beeColony.productInsert(product, images*, taxonomy*, force_update*)
  - beeColony.productUpdate(product, images*, taxonomy*)
  - beeColony.productDelete(sku)

  -- beeColony.ordersGetByStatus(status) ---------------------------------------
  - Get all ubercart orders from database for a single status
  - and return them as an associative array.
  - 
  - param status
  -   String that is equal to ubercart order status. I.E. "Pending" or "Waiting"
  -
  - return
  -   struct Associative array with orders data.

  
  -- beeColony.ordersGetAll() --------------------------------------------------
  - Get all ubercart orders from database and return them as an associative array.
  -
  - return
  -  Associative array with all orders data ordered by order status.
  ------------------------------------------------------------------------------
  
  -- beeColony.orderUpdate(id, data) -------------------------------------------
  - Modify basic data for an order.
  - 
  - param int $id
  -   Order ID number. Assumed as integer value.
  -   
  - param struct $data
  -   An associative array containing values to be modified. Any of following
  -   keys are considered valid:
  -   - NOT ACTIVE - order_status: A string to use for localized order status. It's part of
  -     Ubercart configuration.
  -   - delivery_first_name: A string to use for First Name in delivery address.
  -   - delivery_last_name: A string to use for Last Name in delivery address.
  -   - delivery_phone: A string to use for Phone Number in delivery address.
  -   - delivery_company: A string to use for Company in delivery address
  -   - delivery_street1: A string to use for first line in delivery address.
  -   - delivery_street2: A string to use for last line in delivery address.
  -   - delivery_city: A string to use for City in delivery address.
  -   - delivery_zone: A string to use for Zone in delivery address. Ubercart has
  -     its own zone list, string is checked against list stored in database. 
  -   - delivery_postal_code: A string to use for Postal Code in delivery address.
  -   - delivery_country: A string to use for Country in delivery address.
  -     Ubercart has its own country list, string is checked against list
  -     stored in database.
  -   - billing_first_name: A string to use for First Name in billing address.
  -   - billing_last_name: A string to use for Last Name in billing address.
  -   - billing_phone: A string to use for Phone Number in billing address.
  -   - billing_company: A string to use for Company in billing address
  -   - billing_street1: A string to use for first line in billing address.
  -   - billing_street2: A string to use for last line in billing address.
  -   - billing_city: A string to use for City in billing address.
  -   - billing_zone: A string to use for Zone in billing address. Ubercart
  -     has its own zone list, string is checked versus list stored in database. 
  -   - billing_postal_code: A string to use for Postal Code in billing address.
  -   - billing_country: A string to use for Country in billing address.
  -     Ubercart has its own country list, string is checked against list
  -     stored in database.
  -   - payment_method: A string to use for Payment Method.
  -   - data: Associative array containing various data like VAT numbers.
  -   - status: Product published or not. Must be 0 or 1. Default is 1 (published).
  - @return
  -   Different return values are possible:
  -   - TRUE: If database has been succesfully updated.
  -   - Error Debug Message: If any error has occurred.
  ------------------------------------------------------------------------------
  
  -- beeColony.orderProductsGet(orders) ----------------------------------------
  - Get products for single or multiple orders.
  - 
  - param struct $order_id
  -   Order ID value. It can be a single integer or an array full of integers.
  - 
  - return
  -   Struct - Associative array with product ordered data.
  ------------------------------------------------------------------------------
  
  -- beeColony.orderProductsUpdate(id, sku, data) ------------------------------
  - Modify order products.
  - 
  - Modify model or quantity for any product in order.
  - 
  - param int $id
  -   Order ID number. Assumed as integer value.
  -   
  - param string $sku
  -   Product model. String value.
  - 
  - param struct $data
  -   An associative array containing values to be modified. Any of following keys
  -   is considered valid:
  -   - qty: New quantity for product. Must be positive number.
  -   - model: New model (SKU) for product, all other product data are retrieved from database.
  ------------------------------------------------------------------------------
  
  -- beeColony.ordersGetXML(status) --------------------------------------------
  - Same param as "beeColony.ordersGetByStatus(status)" method. It returns an  
  - XML String. This method is deprecated because its a non-sense to include
  - an XML Struct into another XML Struct (created automatically through XML-RPC)
  - Will be removed in future releases
  ------------------------------------------------------------------------------

  -- beeColony.ordersGetAllXML() -----------------------------------------------
  - Same as "beeColony.ordersGetAll()" method. It returns an  
  - XML String. This method is deprecated because its a non-sense to include
  - an XML Struct into another XML Struct (created automatically through XML-RPC)
  - Will be removed in future releases
  ------------------------------------------------------------------------------
  
  -- beeColony.productGet(sku) -------------------------------------------------
  - Get node structure for single product.
  - 
  - param $sku
  -   String representing product model/SKU
  -   
  - return struct
  -   Associative array that represent $node variable. This contains any
  -   information needed, but not image binary streams.
  ------------------------------------------------------------------------------
  
  -- beeColony.productInsert(product, images*, taxonomy*, force_update*) -------
  -Insert a new product into database.
  -
  -param $product
  -  Associative array containing product data. Any of following keys are
  -  considered valid:
  -  - title: Must be a string. Default is empty string.
  -  - body: Must be a string. Default is empty string.
  -  - teaser: Must be a string. Default is empty string.
  -  - model: Must be a string. It's mandatory to have this key in the array (MANDATORY)
  -  - list_price: Must be an integer. Default is 0.
  -  - cost: Must be an integer. Default is 0.
  -  - sell_price: Must be an integer. Default is 0.
  -  - weight: Must be an integer. Default is 0.
  -  - weight_units: Must be a string. Default is Ubercart default unit.
  -  - length: Must be an integer. Default is 0.
  -  - width: Must be an integer. Default is 0.
  -  - height: Must be an integer. Default is 0.
  -  - length_units: Must be a string. Default is Ubercart default unit.
  -  - pkg_qty: Must be an integer. Default is 1.
  -  - default_qty: Must be an integer. Default is 1.
  -  - ordering: Must be an integer. Default is 0.
  -  - shippable: Must be an integer. Default is 1.
  -  Any other CCK field is supported just use field name (not label) as key.
  -  There is no type control on CCK fields. 
  -  field_image_cache is reserved for $images param, because it needs
  -  different procedures than other fields, so using it in this param gives
  -  an error.
  -
  -param $images
  -  Array containing Associative arrays. Each array must contain structure for
  -  image files:
  -  - file: file data base64 encoded
  -  - filename: file name
  -  - filesize: file size in bytes
  -  - filemime: mime type of img
  -
  -param $taxonomy
  -  Recursive array that represent taxonomy structure for catalog taxonomy tree
  -  Each term contains following keys:
  -  - name: term string (mandatory)
  -  - img: single img for term image (file, filename, filesize and filemime keys, same as $images param) (optional)
  -  - child: recursive term (name, img and child) (optional)
  - 
  -param $force_update
  -  Boolean. If TRUE existant products are directly updated and no errors are
  -  shown if product already exists.
  -   
  -return
  -  TRUE if no errors. service_errors formatted string if any error occurs.
  ------------------------------------------------------------------------------
  
  
  -- beeColony.productUpdate(product, images*, taxonomy*) ----------------------
  - Update an existant product
  -
  - Same data as beeColony.productInsert(product, images*, taxonomy*, force_update*)
  - Only difference is force_update missing (always TRUE)
  ------------------------------------------------------------------------------
  
  -- beeColony.productDelete(sku) ----------------------------------------------
  - Delete a single product.
  - 
  - param $sku
  -   String that represent product model/SKU
  -   
  -return
  -  TRUE if no errors. service_errors formatted string if any error occurs.
  ------------------------------------------------------------------------------

-- CONTACT --

Current maintainers:
* Federico Busetti (FeBus982) - http://drupal.org/user/693478
* Adriano Monaco (GuN_jAcK) - http://drupal.org/user/781748


This project has been sponsored by:
* SKYWAVE
  Specialized in consulting and planning of Drupal powered sites, SKYWAVE offers
  installation, development, theming, customization, and hosting to get you
  started. Visit http://www.skywave.it for more information.
